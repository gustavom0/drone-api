import * as awilix from "awilix";

import { DroneCreator } from "../../api/drones/application/droneCreator";
import { PostgresDroneRepository } from "../../api/drones/infrastructure/postgresDroneRepository";
import { PostgresDroneMedicationRepository } from "../../api/drones/infrastructure/postgresDroneMedicationRepository";
import { PostgresMedicationRepository } from "../../api/medications/infrastructure/postgresMedicationRepository";
import { GetDroneBatterybySerial } from "../../api/drones/application/getDroneBatterybySerial";
import { FindDronesAvailable } from "../../api/drones/application/findDronesAvailable";
import { LogDroneState } from "../../api/drones/application/logDroneState";
import { LoadDroneWithMedications } from "../../api/drones/application/loadDroneWithMedications";
import { FindDroneMedications } from "../../api/drones/application/findDroneMedications";

const container = awilix.createContainer();

container.register({
  droneRepository: awilix.asClass(PostgresDroneRepository).singleton(),
  droneMedicationRepository: awilix.asClass(PostgresDroneMedicationRepository).singleton(),
  medicationRepository: awilix.asClass(PostgresMedicationRepository).singleton(),
  droneCreator: awilix.asClass(DroneCreator),
  getDroneBatterybySerial: awilix.asClass(GetDroneBatterybySerial),
  findDronesAvailable: awilix.asClass(FindDronesAvailable),
  logDroneState: awilix.asClass(LogDroneState),
  loadDroneWithMedications: awilix.asClass(LoadDroneWithMedications),
  findDroneMedications: awilix.asClass(FindDroneMedications),
});

export const droneCreator = container.resolve("droneCreator");
export const getDroneBatterybySerial = container.resolve("getDroneBatterybySerial");
export const findDronesAvailable = container.resolve("findDronesAvailable");
export const logDroneState = container.resolve("logDroneState");
export const loadDroneWithMedications = container.resolve("loadDroneWithMedications");
export const findDroneMedications = container.resolve("findDroneMedications");
