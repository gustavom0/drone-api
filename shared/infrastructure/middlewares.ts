import Joi from 'joi';
import to from 'await-to-js';

import { INext, IReq, IRes } from '../domain/global';

export const handlerValidation = (functionName: string, schemaRule: Joi.ObjectSchema<any>) => async (req: IReq, res: IRes, next: INext) => {
  const reqBody: any = req.body;
  let params = reqBody;

  if (req.method === 'GET') {
    params = req.query;
  }
  if (req.method === 'POST' && reqBody.data) {
    params = JSON.parse(reqBody.data);
  }

  const [error] = await to(schemaRule.validateAsync(params));
  if (error) {
    console.log(functionName, error);

    return res.status(400).send({
      error: {
        message: error.message,
      },
    });
  }

  return next();
};
