FROM node:18-alpine

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm i -g sequelize-cli
# RUN mv models/index2.js models/index.js
# RUN npm run load-seeders
# RUN rm models/index.js

EXPOSE 3000
CMD [ "npm", "start" ]
