import { DataTypes, Model } from 'sequelize';

import sequelize from './index';

class DroneAudit extends Model {
  public id!: number;
  public batteryCapacity!: number;
  public droneId!: number;
}

DroneAudit.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    batteryCapacity: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    droneId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
    },
  },
  {
    sequelize,
    tableName: 'drone_audits',
  }
);

export default DroneAudit;
