import { DataTypes, Model } from 'sequelize';

import sequelize from './index';

class Drone extends Model {
  public id!: number;
  public batteryCapacity!: number;
  public model!: 'Lightweight' | 'Middleweight' | 'Cruiserweight' | 'Heavyweight';
  public serialNumber!: string;
  public state!: 'IDLE' | 'LOADING' | 'LOADED' | 'DELIVERING' | 'DELIVERED' | 'RETURNING';
  public weightLimit!: number;
}

Drone.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    batteryCapacity: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    model: {
      type: DataTypes.ENUM('Lightweight', 'Middleweight', 'Cruiserweight', 'Heavyweight'),
      allowNull: false,
    },
    serialNumber: {
      type: DataTypes.STRING(100),
      allowNull: false,
    },
    state: {
      type: DataTypes.ENUM('IDLE', 'LOADING', 'LOADED', 'DELIVERING', 'DELIVERED', 'RETURNING'),
      allowNull: false,
      defaultValue: 'IDLE',
    },
    weightLimit: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 500,
    },
  },
  {
    sequelize,
    tableName: 'drones',
  }
);

export default Drone;
