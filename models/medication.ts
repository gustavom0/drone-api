import { DataTypes, Model } from 'sequelize';

import sequelize from './index';

class Medication extends Model {
  public id!: number;
  public name!: string;
  public code!: string;
  public image!: string;
  public weight!: number;
}

Medication.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false,
    },
    code: {
      type: DataTypes.STRING(100),
      allowNull: false,
    },
    image: {
      type: DataTypes.STRING(1000),
      allowNull: false,
    },
    weight: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  },
  {
    sequelize,
    tableName: 'medications',
  }
);

export default Medication;
