import { DataTypes, Model } from 'sequelize';

import sequelize from './index';

class DronesMedications extends Model {
  public id!: number;
  public droneId!: number;
  public medicationId!: number;
}

DronesMedications.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    droneId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    medicationId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  },
  {
    sequelize,
    tableName: 'drones_medications',
  }
);

export default DronesMedications;
