import { Sequelize } from 'sequelize';

import Constant from '../shared/constant';

const sequelize = new Sequelize(Constant.DB_NAME, Constant.DB_USER, Constant.DB_PWD, {
  host: 'localhost',
  dialect: 'postgres',
});

export default sequelize;
