import express, { Application } from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import cron from 'node-cron';

import routing from './routing';

import Drone from './models/drone';
import DroneAudit from './models/droneAudit';
import Medication from './models/medication';
import DroneMedication from './models/droneMedication';
import sequelize from './models';

import Constant from './shared/constant';
import { logDroneState } from './shared/infrastructure/dependenciesContainer';

cron.schedule('1 * * * * *', function () {
  logDroneState.run();
});

const app: Application = express();

app.use(cors());
app.use(bodyParser.json());

routing(app);

Drone.hasMany(DroneAudit, { foreignKey: 'droneId' });
Drone.hasMany(DroneMedication, { foreignKey: 'droneId' });
Medication.hasMany(DroneMedication, { foreignKey: 'medicationId' });
DroneMedication.belongsTo(Drone);
DroneMedication.belongsTo(Medication);

sequelize.sync()
  .then(() => {
    app.listen(Constant.PORT, () => {
      console.log(`Server is running on port ${Constant.PORT}`);
    });
  })
  .catch((error) => {
    console.error('Unable to connect to the database:', error);
  });
