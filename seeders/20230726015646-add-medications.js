'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('medications', [
      {
        id: 1,
        name: "Med-master_Standard_Steel_Locking_Medication_Case",
        image: 'https://i.ebayimg.com/images/g/mXMAAOSwBMJfelz5/s-l640.jpg',
        code: 'ASDF_12342',
        weight: 50,
        createdAt: '2023-07-26 01:47:00.39+00',
        updatedAt: '2023-07-26 01:47:00.39+00',
      },
      {
        id: 2,
        name: "Medcosa_28_Days_Pill_Case",
        image: 'https://m.media-amazon.com/images/I/71UBB8B33lL._AC_SL1500_.jpg',
        code: 'ASDF_12343',
        weight: 40,
        createdAt: '2023-07-26 01:47:00.39+00',
        updatedAt: '2023-07-26 01:47:00.39+00',
      },
      {
        id: 3,
        name: "Med-master_Standard_Steel_Locking_Medication_Case",
        image: 'https://i.ebayimg.com/images/g/mXMAAOSwBMJfelz5/s-l640.jpg',
        code: 'ASDF_12344',
        weight: 30,
        createdAt: '2023-07-26 01:47:00.39+00',
        updatedAt: '2023-07-26 01:47:00.39+00',
      },
      {
        id: 4,
        name: "Medcosa_28_Days_Pill_Case",
        image: 'https://m.media-amazon.com/images/I/71UBB8B33lL._AC_SL1500_.jpg',
        code: 'ASDF_12345',
        weight: 60,
        createdAt: '2023-07-26 01:47:00.39+00',
        updatedAt: '2023-07-26 01:47:00.39+00',
      },
      {
        id: 5,
        name: "Med-master_Standard_Steel_Locking_Medication_Case",
        image: 'https://i.ebayimg.com/images/g/mXMAAOSwBMJfelz5/s-l640.jpg',
        code: 'ASDF_12346',
        weight: 55,
        createdAt: '2023-07-26 01:47:00.39+00',
        updatedAt: '2023-07-26 01:47:00.39+00',
      },
    ], {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
