'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('drones', [
      {
        id: 1,
        batteryCapacity: 100,
        model: 'Lightweight',
        serialNumber: '12345qwerasfd',
        state: 'IDLE',
        weightLimit: 150,
        createdAt: '2023-07-26 01:47:00.39+00',
        updatedAt: '2023-07-26 01:47:00.39+00',
      },
      {
        id: 2,
        batteryCapacity: 90,
        model: 'Lightweight',
        serialNumber: '12346qwerasfd',
        state: 'IDLE',
        weightLimit: 200,
        createdAt: '2023-07-26 01:47:00.39+00',
        updatedAt: '2023-07-26 01:47:00.39+00',
      },
      {
        id: 3,
        batteryCapacity: 80,
        model: 'Middleweight',
        serialNumber: '12347qwerasfd',
        state: 'LOADED',
        weightLimit: 250,
        createdAt: '2023-07-26 01:47:00.39+00',
        updatedAt: '2023-07-26 01:47:00.39+00',
      },
      {
        id: 4,
        batteryCapacity: 40,
        model: 'Cruiserweight',
        serialNumber: '12348qwerasfd',
        state: 'IDLE',
        weightLimit: 300,
        createdAt: '2023-07-26 01:47:00.39+00',
        updatedAt: '2023-07-26 01:47:00.39+00',
      },
      {
        id: 5,
        batteryCapacity: 20,
        model: 'Heavyweight',
        serialNumber: '12349qwerasfd',
        state: 'IDLE',
        weightLimit: 350,
        createdAt: '2023-07-26 01:47:00.39+00',
        updatedAt: '2023-07-26 01:47:00.39+00',
      },
    ], {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
