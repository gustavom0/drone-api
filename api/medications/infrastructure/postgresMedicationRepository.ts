import Medication from '../../../models/medication';

import { MedicationRepository } from '../domain/medicationRepository';

export class PostgresMedicationRepository implements MedicationRepository {
  async findById(medicationId: number): Promise<Medication> {
    const medication = await Medication.findOne({
      raw: true,
      where: {
        id: medicationId,
      },
    });

    return medication!;
  }
}
