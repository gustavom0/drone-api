class Medication {
  readonly name: string;
  readonly code: string;
  readonly image: string;
  readonly weight: number;

  constructor(name: string, code: string, image: string, weight: number) {
    this.name = name;
    this.code = code;
    this.image = image;
    this.weight = weight;
  }
}

export default Medication;
