import Medication from './medication';

export interface MedicationRepository {
  findById(medicationId: number): Promise<Medication>;
}
