import { MedicationRepository } from "../../medications/domain/medicationRepository";
import Drone, { DroneModel, DroneState } from "../domain/drone";
import DronesMedication from "../domain/droneMedication";
import { DroneMedicationRepository } from "../domain/droneMedicationRepository";
import { DroneRepository } from "../domain/droneRepository";

export class FindDroneMedications {
  droneRepository: DroneRepository;
  medicationRepository: MedicationRepository;
  droneMedicationRepository: DroneMedicationRepository;

  constructor(dependencies: {
    droneRepository: DroneRepository;
    medicationRepository: MedicationRepository;
    droneMedicationRepository: DroneMedicationRepository;
  }) {
    this.droneRepository = dependencies.droneRepository;
    this.medicationRepository = dependencies.medicationRepository;
    this.droneMedicationRepository = dependencies.droneMedicationRepository;
  }

  async run(params: { serialNumber: string }): Promise<any[]> {
    const drone = await this.droneRepository.findBySerial(params.serialNumber);
    const dronesMedications = await this.droneMedicationRepository.findByDroneId(drone.id) as any;
    const medications: any[] = [];

    for (let i = 0; i < dronesMedications.length; i++) {
      const droneMedication = dronesMedications[i];

      const medication = await this.medicationRepository.findById(droneMedication.medicationId);
      medications.push(medication);
    }

    return medications;
  }
}
