import Drone, { DroneModel, DroneState } from "../domain/drone";
import { DroneRepository } from "../domain/droneRepository";

export class LogDroneState {
  repository: DroneRepository;

  constructor(dependencies: {
    droneRepository: DroneRepository;
  }) {
    this.repository = dependencies.droneRepository
  }

  async run(): Promise<void> {
    await this.repository.registerDroneState();
  }
}
