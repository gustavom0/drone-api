import Drone, { DroneModel, DroneState } from "../domain/drone";
import { DroneRepository } from "../domain/droneRepository";

export class DroneCreator {
  repository: DroneRepository;

  constructor(dependencies: {
    droneRepository: DroneRepository;
  }) {
    this.repository = dependencies.droneRepository
  }

  async run(params: { batteryCapacity: number, model: DroneModel, serialNumber: string, state: DroneState, weightLimit: number }): Promise<void> {
    const drone = new Drone(0, params.batteryCapacity, params.model, params.serialNumber, params.state, params.weightLimit);

    await this.repository.save(drone);
  }
}
