import Drone, { DroneModel, DroneState } from "../domain/drone";
import { DroneRepository } from "../domain/droneRepository";

export class FindDronesAvailable {
  repository: DroneRepository;

  constructor(dependencies: {
    droneRepository: DroneRepository;
  }) {
    this.repository = dependencies.droneRepository
  }

  async run(): Promise<Drone[]> {
    return await this.repository.findDronesAvailable();
  }
}
