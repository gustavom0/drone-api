import Drone, { DroneModel, DroneState } from "../domain/drone";
import { DroneRepository } from "../domain/droneRepository";

export class GetDroneBatterybySerial {
  repository: DroneRepository;

  constructor(dependencies: {
    droneRepository: DroneRepository;
  }) {
    this.repository = dependencies.droneRepository
  }

  async run(params: { serialNumber: string }): Promise<Drone> {
    return await this.repository.findBySerial(params.serialNumber);
  }
}
