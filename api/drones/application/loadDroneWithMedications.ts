import { MedicationRepository } from "../../medications/domain/medicationRepository";
import Drone, { DroneModel, DroneState } from "../domain/drone";
import { DroneMedicationRepository } from "../domain/droneMedicationRepository";
import { DroneRepository } from "../domain/droneRepository";

export class LoadDroneWithMedications {
  droneRepository: DroneRepository;
  medicationRepository: MedicationRepository;
  droneMedicationRepository: DroneMedicationRepository;

  constructor(dependencies: {
    droneRepository: DroneRepository;
    medicationRepository: MedicationRepository;
    droneMedicationRepository: DroneMedicationRepository;
  }) {
    this.droneRepository = dependencies.droneRepository;
    this.medicationRepository = dependencies.medicationRepository;
    this.droneMedicationRepository = dependencies.droneMedicationRepository;
  }

  async run(params: { serialNumber: string, medications: number[] }): Promise<void> {
    let totalWeight = 0;
    for (let i = 0; i < params.medications.length; i++) {
      const medication = params.medications[i];

      const medicationFound = await this.medicationRepository.findById(medication);
      if (!medicationFound) {
        throw new Error(`The medication ${medication} does not exist`);
      }

      totalWeight += medicationFound.weight;
    }

    const droneFound = await this.droneRepository.findBySerial(params.serialNumber);
    if (!droneFound) {
      throw new Error(`The drone ${params.serialNumber} does not exist`);
    }
    if (droneFound.batteryCapacity <= 25) {
      throw new Error(`The drone has not enough battery capacity, the battery percentage is ${droneFound.batteryCapacity}`);
    }
    if(totalWeight > droneFound.weightLimit) {
      throw new Error(`The total weight of the medications is over the drone weight limit`);
    }
    if(droneFound.state !== 'IDLE') {
      throw new Error(`The drone is not available to load with medications`);
    }

    const droneLoadingState = new Drone(droneFound.id, 0, 'Cruiserweight', '', 'LOADING', 0);
    await this.droneRepository.updateDroneState(droneLoadingState);

    for (let i = 0; i < params.medications.length; i++) {
      const medication = params.medications[i];

      await this.droneMedicationRepository.save({
        droneId: droneFound.id,
        medicationId: medication
      });
    }

    const droneLoadedState = new Drone(droneFound.id, 0, 'Cruiserweight', '', 'LOADED', 0);
    await this.droneRepository.updateDroneState(droneLoadedState);
  }
}
