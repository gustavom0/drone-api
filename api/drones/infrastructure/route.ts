import { Router } from 'express';

import * as controller from './controller';

import * as middlewares from '../../../shared/infrastructure/middlewares';

import * as schema from './middlewares/schemas';

const router = Router();

router.post(
  '/drone',
  middlewares.handlerValidation('postDrone', schema.postDrone),
  controller.postDrone,
);

router.post(
  '/drone/:serialNumber/load/medication',
  middlewares.handlerValidation('postLoadMedication', schema.postLoadMedication),
  controller.postLoadMedication,
);

router.get(
  '/drones/available',
  controller.getDronesAvailable,
);

router.get(
  '/drone/:serialNumber',
  controller.getDroneBySerialNumber,
);

router.get(
  '/drone/:serialNumber/medications',
  controller.getDroneMedications,
);

export default router;
