import { IReq, IRes } from '../../../shared/domain/global';
import { droneCreator, getDroneBatterybySerial, findDronesAvailable, loadDroneWithMedications, findDroneMedications } from '../../../shared/infrastructure/dependenciesContainer';

export const postDrone = async (req: IReq, res: IRes) => {
  await droneCreator.run(req.body);

  res.json({ statue: "OK" });
};

export const getDroneBySerialNumber = async (req: IReq, res: IRes) => {
  const drone = await getDroneBatterybySerial.run(req.params);

  res.json({ batteryCapacity: drone.batteryCapacity });
};

export const getDronesAvailable = async (req: IReq, res: IRes) => {
  const drones = await findDronesAvailable.run();

  res.json(drones);
};

export const postLoadMedication = async (req: IReq, res: IRes) => {
  try {
    await loadDroneWithMedications.run({ ...req.params, ...req.body });
    res.json({ statue: "OK" });
  } catch (error: any) {
    res.status(400).json({ statue: "ERROR", message: error.message })
  }
};

export const getDroneMedications = async (req: IReq, res: IRes) => {
  const drones = await findDroneMedications.run(req.params);

  res.json(drones);
};
