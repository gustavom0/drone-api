import Drone from '../../../models/drone';
import DroneAudit from '../../../models/droneAudit';

import { DroneRepository } from '../domain/droneRepository';

export class PostgresDroneRepository implements DroneRepository {
  async save(drone: Drone): Promise<void> {
    const newDrone = await Drone.create(drone as any);
  }

  async findBySerial(serialNumber: string): Promise<Drone> {
    const drone = await Drone.findOne({
      raw: true,
      where: {
        serialNumber,
      },
    });

    return drone!;
  }

  async findDronesAvailable(): Promise<Drone[]> {
    const drones = await Drone.findAll({
      raw: true,
      where: {
        state: 'IDLE',
      },
    });

    return drones;
  }

  async registerDroneState(): Promise<void> {
    const drones = await Drone.findAll({
      raw: true,
    });

    for (let id = 0; id < drones.length; id++) {
      const drone = drones[id];

      await DroneAudit.create({
        droneId: drone.id,
        batteryCapacity: drone.batteryCapacity,
      });
    }
  }

  async updateDroneState(drone: Drone): Promise<void> {
    const droneInstance = await Drone.update({ state: drone.state }, {
      where: {
        id: drone.id,
      },
    });
  }
}
