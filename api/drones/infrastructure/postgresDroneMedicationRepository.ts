import DroneMedication from '../../../models/droneMedication';
import Drone from '../../../models/drone';
import Medication from '../../../models/medication';

import { DroneMedicationRepository } from '../domain/droneMedicationRepository';

export class PostgresDroneMedicationRepository implements DroneMedicationRepository {
  async save(droneMedication: DroneMedication): Promise<void> {
    const newDroneMedication = await DroneMedication.create(droneMedication as any);
  }

  async findByDroneId(droneId: number): Promise<any[]> {
    const dronesMedications = await DroneMedication.findAll({
      raw: true,
      where: {
        droneId,
      },
      include: [
        {
          model: Medication,
        }
      ]
    });

    return dronesMedications;
  }
}
