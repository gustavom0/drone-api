import Joi from 'joi';

export const postDrone = Joi.object({
  batteryCapacity: Joi.number()
    .max(100)
    .min(1)
    .required(),
  model: Joi.string()
    .valid('Lightweight', 'Middleweight', 'Cruiserweight', 'Heavyweight')
    .required(),
  serialNumber: Joi.string()
    .max(100)
    .min(10)
    .required(),
  weightLimit: Joi.number()
    .max(500)
    .min(10)
    .required(),
});

export const postLoadMedication = Joi.object({
  medications: Joi.array()
    .min(1)
    .required(),
});
