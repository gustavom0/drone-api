class DronesMedication {
  readonly droneId: number;
  readonly medicationId: number;

  constructor(droneId: number, medicationId: number) {
    this.droneId = droneId;
    this.medicationId = medicationId;
  }
}

export default DronesMedication;
