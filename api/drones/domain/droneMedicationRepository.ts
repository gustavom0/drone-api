import DroneMedication from './droneMedication';

export interface DroneMedicationRepository {
  save(droneMedication: DroneMedication): Promise<void>;
  findByDroneId(droneId: number): Promise<DroneMedication[]>;
}
