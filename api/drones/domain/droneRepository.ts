import Drone from './drone';

export interface DroneRepository {
  save(drone: Drone): Promise<void>;
  findBySerial(serialNumber: string): Promise<Drone>;
  findDronesAvailable(): Promise<Drone[]>;
  registerDroneState(): Promise<void>;
  updateDroneState(drone: Drone): Promise<void>;
}
