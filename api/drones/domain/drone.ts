export type DroneModel = 'Lightweight' | 'Middleweight' | 'Cruiserweight' | 'Heavyweight';
export  type DroneState = 'IDLE' | 'LOADING' | 'LOADED' | 'DELIVERING' | 'DELIVERED' | 'RETURNING';

class Drone {
  readonly id!: number;
  readonly batteryCapacity: number;
  readonly model: DroneModel;
  readonly serialNumber: string;
  readonly state: DroneState;
  readonly weightLimit: number;

  constructor(id: number, batteryCapacity: number, model: DroneModel, serialNumber: string, state: DroneState, weightLimit: number) {
    this.id = id;
    this.batteryCapacity = batteryCapacity;
    this.model = model;
    this.serialNumber = serialNumber;
    this.state = state;
    this.weightLimit = weightLimit;
  }
}

export default Drone;
